// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {Partition} from './Partition.sol';
import {IERC1400} from './interfaces/IERC1400.sol';
import {ERC1410} from './ERC1410.sol';
import {IERC20} from '@openzeppelin/contracts/token/ERC20/IERC20.sol';

abstract contract ERC1400 is IERC1400, ERC1410 {
    /**
     * @dev Constructor
     *
     * @param beaconAddress The address of the beacon contract pointing to the contract implementing the Series
     */
    constructor(
        address beaconAddress,
        bytes4 initializerSelector
    ) ERC1410(beaconAddress, initializerSelector) {}
}
