// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

abstract contract SerieConstants {
    bytes4 public constant SERIE_INITIALIZER_SELECTOR =
        bytes4(keccak256('_Serie_init(address,uint8)'));
}
