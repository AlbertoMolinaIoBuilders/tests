// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {ERC20SnapshotUpgradeable} from './OZ/ERC20SnapshotUpgradeable.sol';
import {IScheduledSnapshots} from './interfaces/IScheduledSnapshots.sol';

/*
 
    ScheduledSnapshots extends from ERC20SnapshotUpgradeable and adds the concept of "Scheduled Snapshot".
    A schedule snapshot is a snapshot that is meant to be taken at some point in the future, it contains:
        - Scheduled Timestamp : date at which the snapshot is meant to be taken
        - Reference ID : reference that the calling entity has assigned to the scheduled snapshot.

    Scheduled Snapshots are stored in an array in reverse chronological order (of their Scheduled Timestamp): array = [Snapshot far future, ..., Snapsht near future]
    By doing that it will always be possible to schedule a new snapshot before all the already existing one (just extending the array "array.push()"). 
    However, scheduling a snapshot for the far future when there are already multiple snapshots scheduled before that date, might not be possible (transaction will run out of gas).

    Everytime there is a token balance change (transfer, mint, burn), the system will check if there are some scheduled snapshots in the pipe that must be triggered, if so a new snapshot will be taken and the scheduled snapshots removed from the array.
    As a consequence of this, several Scheduled Snapshots might end up referring to the same Snapshot (if several scheduled snapshots were triggered at once).
    It might also happen that, if many scheduled snapshots must be triggered at once, the transaction performing the token balance change runs out of gas.
    In such situation, the only work around is to manually run the "triggerScheduledSnapshots" method indicating how many scheduled snapshots we want to trigger (as many as possible but not too many since the transaction could run out of gas too).
    Please note that, in very exceptional cases, if there are just too many Scheduled Snapshots to be triggered, the "triggerScheduledSnapshots" method must be executed several times.
*/

abstract contract ScheduledSnapshots is
    IScheduledSnapshots,
    ERC20SnapshotUpgradeable
{
    ScheduledSnapshot[] _scheduledSnapshots;

    modifier checkTimestamp(uint256 timestamp) {
        if (timestamp <= block.timestamp) revert('Wrong timestamp');
        _;
    }

    modifier checkIndex(uint256 index) {
        if (index >= _getScheduledSnapshotsCount()) revert('Wrong index');
        _;
    }

    function __ScheduledSnapshots_init(
        string memory initName,
        string memory initSymbol
    ) internal onlyInitializing {
        __ERC20_init(initName, initSymbol);
    }

    function triggerPendingScheduledSnapshots()
        external
        virtual
        override
        returns (uint256)
    {
        return _triggerScheduledSnapshots(0);
    }

    function triggerScheduledSnapshots(
        uint256 max
    ) external virtual override returns (uint256) {
        return _triggerScheduledSnapshots(max);
    }

    function _addScheduledSnapshot(
        uint256 newScheduledTimestamp,
        bytes calldata newData
    ) internal virtual {
        ScheduledSnapshot memory newScheduledSnapshot = ScheduledSnapshot(
            newScheduledTimestamp,
            newData
        );

        uint256 scheduledSnapshots_Length = _getScheduledSnapshotsCount();

        uint256 newScheduledSnapshotId = scheduledSnapshots_Length;

        bool added = false;

        if (scheduledSnapshots_Length == 0)
            _scheduledSnapshots.push(newScheduledSnapshot);
        else {
            for (uint256 j = 1; j <= scheduledSnapshots_Length; j++) {
                uint256 pos = scheduledSnapshots_Length - j;

                if (
                    _scheduledSnapshots[pos].scheduledTimestamp <
                    newScheduledTimestamp
                ) {
                    _slideScheduledSnpashots(pos, scheduledSnapshots_Length);
                } else {
                    newScheduledSnapshotId = pos + 1;
                    _insertScheduledSnpashot(
                        newScheduledSnapshotId,
                        scheduledSnapshots_Length,
                        newScheduledSnapshot
                    );
                    added = true;
                    break;
                }
            }

            if (!added)
                _insertScheduledSnpashot(
                    0,
                    scheduledSnapshots_Length,
                    newScheduledSnapshot
                );
        }
    }

    function _triggerScheduledSnapshots(
        uint256 max
    ) internal virtual returns (uint256) {
        uint256 scheduledSnapshots_Length = _getScheduledSnapshotsCount();

        if (scheduledSnapshots_Length == 0) return 0;

        uint256 _max = max;

        uint256 newSnapShotID;

        if (max > scheduledSnapshots_Length || max == 0)
            _max = scheduledSnapshots_Length;

        for (uint256 j = 1; j <= scheduledSnapshots_Length; j++) {
            uint256 pos = scheduledSnapshots_Length - j;

            ScheduledSnapshot
                memory currentScheduledSnapshot = _scheduledSnapshots[pos];

            if (currentScheduledSnapshot.scheduledTimestamp < block.timestamp) {
                if (pos == scheduledSnapshots_Length - 1)
                    newSnapShotID = _snapshot();

                _scheduledSnapshots.pop();

                _onScheduledSnapshotTriggered(
                    newSnapShotID,
                    currentScheduledSnapshot.data
                );
            } else break;
        }

        return newSnapShotID;
    }

    function _getScheduledSnapshotsCount()
        internal
        view
        virtual
        returns (uint256)
    {
        return _scheduledSnapshots.length;
    }

    function _getScheduledSnapshotsByIndex(
        uint256 index
    ) internal view virtual returns (ScheduledSnapshot memory) {
        return _scheduledSnapshots[index];
    }

    function _slideScheduledSnpashots(uint256 pos, uint256 length) private {
        if (pos == length - 1)
            _scheduledSnapshots.push(_scheduledSnapshots[pos]);
        else {
            _scheduledSnapshots[pos + 1]
                .scheduledTimestamp = _scheduledSnapshots[pos]
                .scheduledTimestamp;
            _scheduledSnapshots[pos + 1].data = _scheduledSnapshots[pos].data;
        }
    }

    function _insertScheduledSnpashot(
        uint256 pos,
        uint256 length,
        ScheduledSnapshot memory scheduledSnapshotToInsert
    ) private {
        if (pos == length) _scheduledSnapshots.push(scheduledSnapshotToInsert);
        else {
            _scheduledSnapshots[pos]
                .scheduledTimestamp = scheduledSnapshotToInsert
                .scheduledTimestamp;
            _scheduledSnapshots[pos].data = scheduledSnapshotToInsert.data;
        }
    }

    function _onScheduledSnapshotTriggered(
        uint256 snapShotID,
        bytes memory data
    ) internal virtual {}

    // Trigger scheduled snapshots if there are any before actually updating them (as implemented in the parent contract)
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        _triggerScheduledSnapshots(0);
        super._beforeTokenTransfer(from, to, amount);
    }
}
