// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

interface IScheduledSnapshots {
    /**
     * @dev scheduled snapshots triggers a snapshot as soons as any balance change is performed after the schedule timestamp.
     * Once the snapshot is triggered a hook function will be executed, the function will receive the snapshot and the data originally provided.
     *
     * @params scheduledTimestamp the date after which any balance change will trigger a snapshot.
     * @params data defines the data that will be provided to the hook function executed after the scheduled snapshot is triggered.
     */

    struct ScheduledSnapshot {
        uint256 scheduledTimestamp;
        bytes data;
    }

    function takeSnapshot() external returns (uint256);

    function addScheduledSnapshot(
        uint256 newScheduledTimestamp,
        bytes calldata newData
    ) external;

    function triggerPendingScheduledSnapshots() external returns (uint256);

    function triggerScheduledSnapshots(uint256 max) external returns (uint256);
}
