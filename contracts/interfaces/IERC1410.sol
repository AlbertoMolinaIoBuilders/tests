// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

interface IERC1410 {
    /////////////////////////////////////////////////////////////////////
    // ERC-20 Extension /////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function totalSupplyByPartition(
        uint256 partitionId
    ) external view returns (uint256);

    function decimalsByPartition(
        uint256 partitionId
    ) external view returns (uint8);

    function balanceOfByPartition(
        uint256 partitionId,
        address tokenHolder
    ) external view returns (uint256);

    /*function approveByPartition(
        bytes32 _partition,
        address _spender,
        uint256 _value
    ) external returns (bool success);

    function allowanceByPartition(
        bytes32 _partition,
        address _owner,
        address _spender
    ) external view returns (uint256 remaining);

    function getApprovedSpendersByPartition(
        bytes32 _partition,
        address _owner
    ) external view returns (address[] memory);*/
    /////////////////////////////////////////////////////////////////////
    // Token Information ////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /*function partitionsOf(
        address _tokenHolder
    ) external view returns (bytes32[] memory);*/
    /////////////////////////////////////////////////////////////////////
    // Partition Token Transfers ////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /*function transferByPartition(
        bytes32 _partition,
        address _to,
        uint256 _value,
        bytes memory _data
    ) external returns (bytes32);

    function transferFromByPartition(
        bytes32 _partition,
        address _from,
        address _to,
        uint256 _value,
        bytes memory _data
    ) external returns (bytes32);

    function operatorTransferByPartition(
        bytes32 _partition,
        address _from,
        address _to,
        uint256 _value,
        bytes memory _data,
        bytes memory _operatorData
    ) external returns (bytes32); // Can also be used by Controllers*/
    /////////////////////////////////////////////////////////////////////
    // Transfer Validity ////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /*function canTransferByPartition(
        address _from,
        address _to,
        bytes32 _partition,
        uint256 _value,
        bytes memory _data
    ) external view returns (bytes memory, bytes32, bytes32);*/
    /////////////////////////////////////////////////////////////////////
    // Operator Management //////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /*function authorizeOperatorByPartition(
        bytes32 _partition,
        address _operator
    ) external;

    function revokeOperatorByPartition(
        bytes32 _partition,
        address _operator
    ) external;

    function isOperatorForPartition(
        bytes32 _partition,
        address _operator,
        address _tokenHolder
    ) external view returns (bool);

    function getOperatorsForPartition(
        bytes32 _partition,
        address _tokenHolder
    ) external view returns (address[] memory);*/
    /**
     * AuthorizeOperator / RevokeOperatorfrom / isOperator from ALL partitions of * the "msg.sender".
     */
    /*function authorizeOperator(address _operator) external;

    function revokeOperator(address _operator) external;

    function isOperator(
        address _operator,
        address _tokenHolder
    ) external view returns (bool);*/
    /////////////////////////////////////////////////////////////////////
    // Token Issuance ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function issueByPartition(
        uint256 partitionId,
        address tokenHolder,
        uint256 value,
        bytes memory data
    ) external;
    /////////////////////////////////////////////////////////////////////
    // Token Redemption /////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /*function redeemByPartition(
        bytes32 _partition,
        uint256 _value,
        bytes memory _data
    ) external;

    function operatorRedeemByPartition(
        bytes32 _partition,
        address _tokenHolder,
        uint256 _value,
        bytes memory _operatorData
    ) external; // Can also be used by Controllers*/
}
