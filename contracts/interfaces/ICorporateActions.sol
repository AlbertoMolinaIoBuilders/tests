// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

/**
 *
 */
interface ICorporateActions {
    /**
     * @dev a corporate action is defined by its "properties" and can be classified by its "type".
     * corporate actions can trigger on-chain events such as snapshots, splits etc...
     * The outcome of those events can be dynamically stored in "results".
     *  - properties and type are provided by the action creator and are not supposed to change after the action is created.
     *  - results are meant to be added gradually by smart contracts (although they could also be added by external users), as the events triggered by the action are executed.
     *
     * @params actionType defines the corporate action type
     * @params properties defines the corporate action type, any structure can be used
     * @params results list of outcomes/results related to the corporate action
     */
    struct CorporateAction {
        bytes32 actionType;
        bytes properties;
        bytes[] results;
    }
}
