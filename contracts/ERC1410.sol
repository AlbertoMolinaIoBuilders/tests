// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {Partition} from './Partition.sol';
import {IERC1410} from './interfaces/IERC1410.sol';
import {IPartition} from './interfaces/IPartition.sol';
import {IERC20} from '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import {BeaconProxy} from '@openzeppelin/contracts/proxy/beacon/BeaconProxy.sol';

abstract contract ERC1410 is IERC1410, IERC20 {
    mapping(address => IPartition.PartitionMetadata) internal _partitions;
    address[] internal _listOfPartitions;
    address internal _beaconAddress;
    bytes4 internal _initializerSelector;

    /**
     * @dev Constructor
     *
     * @param beaconAddress The address of the beacon contract pointing to the contract implementing the Series
     */
    constructor(address beaconAddress, bytes4 initializerSelector) {
        _beaconAddress = beaconAddress;
        _initializerSelector = initializerSelector;
    }

    /////////////////////////////////////////////////////////////////////
    // MODIFIER ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    modifier checkPartitionExists(address partitionAddress) {
        _checkPartitionExists(partitionAddress);
        _;
    }

    function _checkPartitionExists(address partitionAddress) internal view {
        if (_getPartitionId(partitionAddress) == 0)
            revert('partition does not exist');
    }

    modifier checkPartitionIdExists(uint256 partitionId) {
        _checkPartitionIdExists(partitionId);
        _;
    }

    function _checkPartitionIdExists(uint256 partitionId) internal view {
        if (partitionId == 0) revert('partition id must be greater than 0');
        if (partitionId > _getPartitionsCount())
            revert('partition id does not exist');
    }

    /////////////////////////////////////////////////////////////////////
    // Partitions Management ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function _addPartition(
        bytes memory metadata,
        bytes memory initializationArguments
    ) internal virtual returns (bool, uint256) {
        bytes memory initializationData = abi.encodePacked(
            _initializerSelector,
            initializationArguments
        );

        address newPartitionAddress = address(
            new BeaconProxy(_beaconAddress, initializationData)
        );

        _listOfPartitions.push(newPartitionAddress);

        IPartition.PartitionMetadata memory newPartitionMetadata;
        newPartitionMetadata.id = _getPartitionsCount();
        newPartitionMetadata.metadata = metadata;

        _partitions[newPartitionAddress] = newPartitionMetadata;

        return (true, newPartitionMetadata.id);
    }

    function _updatePartition(
        address partitionAddress,
        bytes memory metadata
    ) internal virtual returns (bool) {
        _partitions[partitionAddress].metadata = metadata;
        return true;
    }

    function _getPartitionsCount() internal view virtual returns (uint256) {
        return _listOfPartitions.length;
    }

    function _getPartition(
        address partitionAddress
    ) internal view virtual returns (bytes memory) {
        return _partitions[partitionAddress].metadata;
    }

    function _getPartitionAddress(
        uint256 _partitionIndex
    ) internal view virtual returns (address) {
        return _listOfPartitions[_partitionIndex];
    }

    function _getPartitionId(
        address partitionAddress
    ) internal view virtual returns (uint256) {
        return _partitions[partitionAddress].id;
    }

    /////////////////////////////////////////////////////////////////////
    // Token Issuance ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function issueByPartition(
        uint256 partitionId,
        address tokenHolder,
        uint256 value,
        bytes memory data
    ) public virtual override checkPartitionIdExists(partitionId) {
        IPartition(_getPartitionAddress(partitionId - 1)).issue(
            tokenHolder,
            value
        );
    }

    /////////////////////////////////////////////////////////////////////
    // ERC 20 Compatibility ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function decimals() external view returns (uint8) {
        uint8 commonDecimals = 0;

        for (
            uint256 partitionIndex = 0;
            partitionIndex < _getPartitionsCount();
            partitionIndex++
        ) {
            uint8 partitionDecimals = _decimalsByPartition(partitionIndex);

            (, commonDecimals) = _addWithDecimals(
                0,
                commonDecimals,
                0,
                partitionDecimals
            );
        }

        return commonDecimals;
    }

    function totalSupply() external view override returns (uint256) {
        uint256 totalTotalSupply = 0;
        uint8 commonDecimals = 0;

        for (
            uint256 partitionIndex = 0;
            partitionIndex < _getPartitionsCount();
            partitionIndex++
        ) {
            uint256 partitionTotalSupply = _totalSupplyByPartition(
                partitionIndex
            );
            uint8 partitionDecimals = _decimalsByPartition(partitionIndex);

            (totalTotalSupply, commonDecimals) = _addWithDecimals(
                totalTotalSupply,
                commonDecimals,
                partitionTotalSupply,
                partitionDecimals
            );
        }

        return totalTotalSupply;
    }

    function balanceOf(
        address account
    ) external view override returns (uint256) {
        uint256 totalBalance = 0;
        uint8 commonDecimals = 0;

        for (
            uint256 partitionIndex = 0;
            partitionIndex < _getPartitionsCount();
            partitionIndex++
        ) {
            uint256 partitionBalance = _balanceOfByPartition(
                partitionIndex,
                account
            );
            uint8 partitionDecimals = _decimalsByPartition(partitionIndex);

            (totalBalance, commonDecimals) = _addWithDecimals(
                totalBalance,
                commonDecimals,
                partitionBalance,
                partitionDecimals
            );
        }

        return totalBalance;
    }

    function _addWithDecimals(
        uint256 totalAmount,
        uint8 commonDecimals,
        uint256 newAmount,
        uint8 newDecimals
    ) internal pure returns (uint256, uint8) {
        if (newDecimals < commonDecimals)
            newAmount *= 10 ** (commonDecimals - newDecimals);
        else if (newDecimals > commonDecimals) {
            totalAmount *= 10 ** (newDecimals - commonDecimals);
            commonDecimals = newDecimals;
        }

        totalAmount += newAmount;

        return (totalAmount, commonDecimals);
    }

    /////////////////////////////////////////////////////////////////////
    // ERC-20 Extension /////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    function totalSupplyByPartition(
        uint256 partitionId
    ) external view checkPartitionIdExists(partitionId) returns (uint256) {
        return _totalSupplyByPartition(partitionId - 1);
    }

    function _totalSupplyByPartition(
        uint256 partitionIndex
    ) internal view returns (uint256) {
        return IERC20(_getPartitionAddress(partitionIndex)).totalSupply();
    }

    function decimalsByPartition(
        uint256 partitionId
    ) external view checkPartitionIdExists(partitionId) returns (uint8) {
        return _decimalsByPartition(partitionId - 1);
    }

    function _decimalsByPartition(
        uint256 partitionIndex
    ) internal view returns (uint8) {
        return Partition(_getPartitionAddress(partitionIndex)).decimals();
    }

    function balanceOfByPartition(
        uint256 partitionId,
        address tokenHolder
    ) external view checkPartitionIdExists(partitionId) returns (uint256) {
        return _balanceOfByPartition(partitionId - 1, tokenHolder);
    }

    function _balanceOfByPartition(
        uint256 partitionIndex,
        address tokenHolder
    ) internal view returns (uint256) {
        return
            IERC20(_getPartitionAddress(partitionIndex)).balanceOf(tokenHolder);
    }
}
