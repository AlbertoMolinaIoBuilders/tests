// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

abstract contract SecurityConstants {
    uint256 internal constant SNAPSHOT_LIST_ID = 0;
    uint256 internal constant SNAPSHOT_RESULT_ID = 1;
}
